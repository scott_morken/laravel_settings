<?php

namespace Tests\Smorken\Settings\Integration\Storage;

use Carbon\Carbon;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\MySqlConnection;
use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\CacheAssist\CacheAssist;
use Smorken\Settings\Models\Eloquent\Setting;
use Tests\Smorken\Settings\TestCase;

class SettingTest extends TestCase
{
    public function testGet(): void
    {
        [$sut, $pdo] = $this->getSut();
        $stmt = m::mock(\PDOStatement::class);
        $stmt->shouldReceive('setFetchMode');
        $stmt->shouldReceive('bindValue')
            ->once()
            ->with(1, 'foo.key', 2);
        $stmt->shouldReceive('execute')
            ->once();
        $stmt->shouldReceive('fetchAll')
            ->once()
            ->andReturn([['key' => 'foo.key', 'value' => 'foo.value']]);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('select * from `settings` where `key` = ? limit 1')
            ->andReturn($stmt);
        $sut->getCacheAssist()->getCache()->shouldReceive('has')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/setting/fookey')
            ->andReturn(false);
        $sut->getCacheAssist()->getCache()->shouldReceive('put')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/setting/fookey', 'foo.value', m::type(Carbon::class))
            ->andReturn(true);
        $this->assertEquals('foo.value', $sut->get('foo.key'));
    }

    public function testGetWithNoModelReturnsDefault(): void
    {
        [$sut, $pdo] = $this->getSut();
        $stmt = m::mock(\PDOStatement::class);
        $stmt->shouldReceive('setFetchMode');
        $stmt->shouldReceive('bindValue')
            ->once()
            ->with(1, 'foo.key', 2);
        $stmt->shouldReceive('execute')
            ->once();
        $stmt->shouldReceive('fetchAll')
            ->once()
            ->andReturn([]);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('select * from `settings` where `key` = ? limit 1')
            ->andReturn($stmt);
        $sut->getCacheAssist()->getCache()->shouldReceive('has')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/setting/fookey')
            ->andReturn(false);
        $sut->getCacheAssist()->getCache()->shouldReceive('put')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/setting/fookey', 'default', m::type(Carbon::class))
            ->andReturn(true);
        $this->assertEquals('default', $sut->get('foo.key', 'default'));
    }

    public function testSetExisting(): void
    {
        [$sut, $pdo] = $this->getSut();
        $stmt = m::mock(\PDOStatement::class);
        $stmt->shouldReceive('setFetchMode');
        $stmt->shouldReceive('bindValue')
            ->once()
            ->with(1, 'foo.key', 2);
        $stmt->shouldReceive('execute')
            ->once();
        $stmt->shouldReceive('fetchAll')
            ->once()
            ->andReturn([['id' => 1, 'key' => 'foo.key', 'descr' => 'foo.key', 'value' => 'foo.value']]);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('select * from `settings` where `key` = ? limit 1')
            ->andReturn($stmt);
        $istmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('update `settings` set `value` = ?, `settings`.`updated_at` = ? where `id` = ?')
            ->andReturn($istmt);
        $istmt->shouldReceive('bindValue')
            ->with(1, 'bar', 2);
        $istmt->shouldReceive('bindValue')
            ->with(2, m::type('string'), 2);
        $istmt->shouldReceive('bindValue')
            ->with(3, 1, 1);
        $istmt->shouldReceive('execute')
            ->once();
        $istmt->shouldReceive('rowCount')
            ->once()
            ->andReturn(1);
        $cstmt = m::mock(\PDOStatement::class);
        $cstmt->shouldReceive('setFetchMode');
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('select * from `settings` order by `key` asc')
            ->andReturn($cstmt);
        $cstmt->shouldReceive('execute')
            ->once();
        $cstmt->shouldReceive('fetchAll')
            ->once()
            ->andReturn([['id' => 1, 'key' => 'foo.key', 'descr' => 'foo.key', 'value' => 'bar']]);
        $sut->getCacheAssist()->getCache()->shouldReceive('has')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/all')
            ->andReturn(false);
        $sut->getCacheAssist()->getCache()->shouldReceive('put')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/all', m::type(Collection::class), m::type(Carbon::class))
            ->andReturn(true);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'SmorkenSettingsStorageEloquentSetting/setting/fookey'
        )->andReturn(1);
        $this->assertTrue($sut->set('foo.key', 'bar'));
    }

    public function testSetNew(): void
    {
        [$sut, $pdo] = $this->getSut();
        $stmt = m::mock(\PDOStatement::class);
        $stmt->shouldReceive('setFetchMode');
        $stmt->shouldReceive('bindValue')
            ->once()
            ->with(1, 'foo.key', 2);
        $stmt->shouldReceive('execute')
            ->once();
        $stmt->shouldReceive('fetchAll')
            ->once()
            ->andReturn([]);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('select * from `settings` where `key` = ? limit 1')
            ->andReturn($stmt);
        $istmt = m::mock(\PDOStatement::class);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with(
                'insert into `settings` (`key`, `descr`, `value`, `updated_at`, `created_at`) values (?, ?, ?, ?, ?)'
            )
            ->andReturn($istmt);
        $istmt->shouldReceive('bindValue')
            ->with(1, 'foo.key', 2);
        $istmt->shouldReceive('bindValue')
            ->with(2, 'foo.key', 2);
        $istmt->shouldReceive('bindValue')
            ->with(3, 'bar', 2);
        $istmt->shouldReceive('bindValue')
            ->with(4, m::type('string'), 2);
        $istmt->shouldReceive('bindValue')
            ->with(5, m::type('string'), 2);
        $istmt->shouldReceive('execute')
            ->once();
        $pdo->shouldReceive('lastInsertId')
            ->once()
            ->andReturn(1);
        $cstmt = m::mock(\PDOStatement::class);
        $cstmt->shouldReceive('setFetchMode');
        $pdo->shouldReceive('prepare')
            ->once()
            ->with('select * from `settings` order by `key` asc')
            ->andReturn($cstmt);
        $cstmt->shouldReceive('execute')
            ->once();
        $cstmt->shouldReceive('fetchAll')
            ->once()
            ->andReturn([['id' => 1, 'key' => 'foo.key', 'descr' => 'foo.key', 'value' => 'bar']]);
        $sut->getCacheAssist()->getCache()->shouldReceive('has')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/all')
            ->andReturn(false);
        $sut->getCacheAssist()->getCache()->shouldReceive('put')
            ->once()
            ->with('SmorkenSettingsStorageEloquentSetting/all', m::type(Collection::class), m::type(Carbon::class))
            ->andReturn(true);
        $sut->getCacheAssist()->getCache()->shouldReceive('forget')->once()->with(
            'SmorkenSettingsStorageEloquentSetting/setting/fookey'
        )->andReturn(1);
        $this->assertTrue($sut->set('foo.key', 'bar'));
    }

    protected function getSut(): array
    {
        $pdo = m::mock(\PDO::class);
        $connections = [
            'test' => new MySqlConnection($pdo),
        ];
        $resolver = new ConnectionResolver($connections);
        $resolver->setDefaultConnection('test');
        Setting::setConnectionResolver($resolver);
        $sut = new \Smorken\Settings\Storage\Eloquent\Setting(new Setting());

        return [$sut, $pdo];
    }

    protected function setUp(): void
    {
        parent::setUp();
        CacheAssist::mock();
    }

    protected function tearDown(): void
    {
        m::close();
    }
}
