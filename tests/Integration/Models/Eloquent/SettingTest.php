<?php

namespace Tests\Smorken\Settings\Integration\Models\Eloquent;

use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\MySqlConnection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\CacheAssist\CacheAssist;
use Smorken\Settings\Models\Eloquent\Setting;

class SettingTest extends TestCase
{
    public function testKeyIs(): void
    {
        $data = [
            'id' => 1,
            'key' => 'foo.key',
            'descr' => 'foo',
            'value' => 'bar',
        ];
        [$sut, $pdo, $stmt] = $this->getSut();
        $pdo->shouldReceive('prepare')->once()->with('select * from `settings` where `key` = ? limit 1')->andReturn(
            $stmt
        );
        $stmt->shouldReceive('bindValue')->once()->with(1, 'foo.key', 2);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetchAll')->once()->andReturn(
            [
                $data,
            ]
        );
        $m = $sut->keyIs('foo.key')->first();
        $this->assertEquals($data, $m->toArray());
    }

    protected function getSut(): array
    {
        $pdo = m::mock(\PDO::class);
        $stmt = m::mock(\PDOStatement::class);
        $stmt->shouldReceive('setFetchMode');
        $connections = [
            'test' => new MySqlConnection($pdo),
        ];
        $resolver = new ConnectionResolver($connections);
        $resolver->setDefaultConnection('test');
        Setting::setConnectionResolver($resolver);
        $sut = new Setting();

        return [$sut, $pdo, $stmt];
    }

    protected function setUp(): void
    {
        parent::setUp();
        date_default_timezone_set('UTC');
        Setting::unguard();
        CacheAssist::mock();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Setting::reguard();
    }
}
