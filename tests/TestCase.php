<?php

namespace Tests\Smorken\Settings;

use Illuminate\Contracts\Config\Repository;
use Smorken\Settings\ServiceProvider;

abstract class TestCase extends \Orchestra\Testbench\TestCase
{
    public $baseUrl = 'http://localhost';

    protected function defineEnvironment($app)
    {
        tap($app['config'], function (Repository $config) {
            $config->set('app.env', 'production');
            $config->set('app.debug', false);
            $config->set('mail.mailer', 'log');
            $config->set('database.default', 'testbench');
            $config->set(
                'database.connections.testbench',
                [
                    'driver' => 'sqlite',
                    'database' => ':memory:',
                    'prefix' => '',
                ]
            );
            $config->set('settings.middleware', ['web', 'auth']);
            $config->set('view.paths', [__DIR__.'/../views']);
        });
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
            \Smorken\CacheAssist\ServiceProvider::class,
            \Smorken\ArrayCache\ServiceProvider::class,
        ];
    }
}
