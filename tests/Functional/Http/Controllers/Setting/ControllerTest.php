<?php

namespace Tests\Smorken\Settings\Functional\Http\Controllers\Setting;

use Illuminate\Foundation\Auth\User;
use Smorken\Settings\Models\Eloquent\Setting;
use Tests\Smorken\Settings\TestCase;

class ControllerTest extends TestCase
{
    public function testClearCache(): void
    {
        $s = Setting::factory()->create();
        $user = new User();
        $response = $this->actingAs($user)
            ->get('/admin/settings/clear-cache');
        $response->assertRedirect('/admin/settings');
    }

    public function testCreateFailsValidation(): void
    {
        $user = new User();
        $this->actingAs($user)
            ->get('/admin/settings/create');
        $response = $this->actingAs($user)
            ->post('/admin/settings/save', ['key' => 'foo.key']);
        $response->assertRedirect('/admin/settings/create')
            ->assertSessionHasErrors(['value' => 'The value field is required.']);
    }

    public function testCreateSuccess(): void
    {
        $user = new User();
        $this->actingAs($user)
            ->get('/admin/settings/create');
        $response = $this->actingAs($user)
            ->post('/admin/settings/save',
                ['key' => 'foo.key', 'descr' => 'foo key descr', 'value' => 'foo key value']);
        $response->assertRedirect('/admin/settings');
    }

    public function testDelete(): void
    {
        $s = Setting::factory()->create();
        $user = new User();
        $response = $this->actingAs($user)
            ->delete('/admin/settings/delete/'.$s->id);
        $response->assertRedirect('/admin/settings');
        $response = $this->actingAs($user)
            ->get('/admin/settings');
        $response->assertSeeText('No records found');
    }

    public function testIndexNoRecords(): void
    {
        $this->withoutMiddleware();
        $response = $this->get('/admin/settings');
        $response->assertSeeText('No records found');
    }

    public function testIndexWithRecords(): void
    {
        $s = Setting::factory()->create();
        $this->withoutMiddleware();
        $response = $this->get('/admin/settings');
        $response->assertSeeText($s->key)
            ->assertSee('title="Update '.$s->getKey(), false);
    }

    public function testUpdate(): void
    {
        $s = Setting::factory()->create();
        $user = new User();
        $this->actingAs($user)
            ->get('/admin/settings/update/'.$s->id);
        $response = $this->actingAs($user)
            ->post('/admin/settings/save/'.$s->id, ['value' => 'bar bar bar']);
        $response->assertRedirect('/admin/settings');
        $response = $this->actingAs($user)
            ->get('/admin/settings');
        $response->assertSeeText('bar bar bar');
    }

    public function testUpdateFailsValidation(): void
    {
        $s = Setting::factory()->create();
        $user = new User();
        $this->actingAs($user)
            ->get('/admin/settings/update/'.$s->id);
        $response = $this->actingAs($user)
            ->post('/admin/settings/save/'.$s->id, ['value' => '']);
        $response->assertRedirect('/admin/settings/update/'.$s->id)
            ->assertSessionHasErrors(['value' => 'The value field is required.']);
    }

    public function testView(): void
    {
        $s = Setting::factory()->create();
        $this->withoutMiddleware();
        $response = $this->get('/admin/settings/view/'.$s->id);
        $response->assertSeeText($s->key)
            ->assertSeeText($s->value)
            ->assertSee('title="Update '.$s->getKey(), false);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate');
    }
}
