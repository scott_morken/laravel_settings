<?php

namespace Tests\Smorken\Settings\Unit\Services;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\ArrayCache\Contracts\Service;
use Smorken\ArrayCache\Repository;
use Smorken\Settings\Contracts\Storage\Setting;
use Smorken\Settings\Services\SettingService;

class SettingServiceTest extends TestCase
{
    public function testGetCanRetrieveCachedValueFromGet(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('get')
            ->once()
            ->with('foo', null)
            ->andReturn('biz');
        $this->assertEquals('biz', $sut->get('foo')->value);
        $this->assertEquals('biz', $sut->get('foo')->value);
    }

    public function testGetCanRetrieveCachedValueFromSet(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('set')
            ->once()
            ->with('foo', 'bar')
            ->andReturn(true);
        $sut->getProvider()->shouldReceive('get')
            ->never();
        $sut->set('foo', 'bar');
        $this->assertEquals('bar', $sut->get('foo')->value);
    }

    public function testGetCanReturnValue(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('get')
            ->once()
            ->with('foo', 'bar')
            ->andReturn('biz');
        $this->assertEquals('biz', $sut->get('foo', 'bar')->value);
    }

    public function testSetCanSetValue(): void
    {
        $sut = $this->getSut();
        $sut->getProvider()->shouldReceive('set')
            ->once()
            ->with('foo', 'bar')
            ->andReturn(true);
        $this->assertTrue($sut->set('foo', 'bar'));
    }

    protected function getSut(): \Smorken\Settings\Contracts\Services\SettingService
    {
        return new SettingService(m::mock(Setting::class), [
            Service::class => new \Smorken\ArrayCache\Service(Repository::getInstance()),
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Repository::reset();
    }
}
