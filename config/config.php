<?php

return [
    'subnav' => 'admin',
    'middleware' => ['web', 'auth', 'can:role-admin'],
    'controller' => \Smorken\Settings\Http\Controllers\Controller::class,
    'load_routes' => true,
    'model' => \Smorken\Settings\Models\Eloquent\Setting::class,
    'provider' => \Smorken\Settings\Storage\Eloquent\Setting::class,
];
