<?php

namespace Smorken\Settings\Routes;

use Illuminate\Routing\Router;
use Smorken\Support\LoadRoutes;

class AdminRoutes extends LoadRoutes
{
    protected function loadRoutes(Router $router): void
    {
        $router->get('/', 'index');
        $router->get('/create', 'create');
        $router->get('/view/{id}', 'view');
        $router->get('/update/{id}', 'update');
        $router->post('/save/{id?}', 'doSave');
        $router->get('/delete/{id}', 'delete');
        $router->delete('/delete/{id}', 'doDelete');
        $router->get('/clear-cache', 'clearCache');
    }
}
