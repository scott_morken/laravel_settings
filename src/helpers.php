<?php

if (! function_exists('setting')) {
    function setting(?string $key = null, mixed $default = null): mixed
    {
        $s = \Illuminate\Support\Facades\App::make('settings');
        if (! is_null($key)) {
            return $s->get($key, $default);
        }

        return $s;
    }
}
