<?php

namespace Smorken\Settings\Services;

use Illuminate\Http\Request;

class RequestService extends \Smorken\Service\Services\RequestService
{
    protected function modifyRequest(Request $request): Request
    {
        return $request->merge([
            'descr' => $request->input('descr') ?? '',
        ]);
    }
}
