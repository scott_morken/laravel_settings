<?php

namespace Smorken\Settings\Services;

use Smorken\ArrayCache\Extensions\HasArrayCacheService;
use Smorken\Service\Services\BaseService;
use Smorken\Settings\Contracts\Services\SettingResult;
use Smorken\Settings\Contracts\Storage\Setting;

class SettingService extends BaseService implements \Smorken\Settings\Contracts\Services\SettingService
{
    use HasArrayCacheService;

    public function __construct(protected Setting $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function flushCache(): bool
    {
        return $this->getProvider()->flushAll();
    }

    public function get(string $key, mixed $default = null): SettingResult
    {
        $value = $this->getSetting($key, $default);

        return new \Smorken\Settings\Services\SettingResult($key, $value);
    }

    public function getProvider(): Setting
    {
        return $this->provider;
    }

    public function set(string $key, mixed $value): bool
    {
        if ($this->getProvider()->set($key, $value)) {
            $this->toCache($key, $value);

            return true;
        }

        return false;
    }

    protected function getSetting(string $key, mixed $default = null): mixed
    {
        if ($this->hasCachedKey($key)) {
            return $this->fromCache($key) ?? $default;
        }
        $value = $this->getProvider()->get($key, $default);
        $this->toCache($key, $value);

        return $value;
    }
}
