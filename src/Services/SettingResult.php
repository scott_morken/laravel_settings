<?php

namespace Smorken\Settings\Services;

use Smorken\Service\Services\VO\VOResult;

class SettingResult extends VOResult implements \Smorken\Settings\Contracts\Services\SettingResult
{
    public function __construct(
        public string $key,
        public mixed $value
    ) {
    }
}
