<?php

namespace Smorken\Settings\Services;

use Smorken\Service\Contracts\Services\CreateService;
use Smorken\Service\Contracts\Services\DeleteService;
use Smorken\Service\Contracts\Services\RetrieveService;
use Smorken\Service\Contracts\Services\UpdateService;
use Smorken\Settings\Contracts\Services\SettingService;

class CrudServices extends \Smorken\Service\Services\CrudByStorageProviderServices implements \Smorken\Settings\Contracts\Services\CrudServices
{
    protected array $services = [
        SettingService::class => null,
        CreateService::class => null,
        DeleteService::class => null,
        RetrieveService::class => null,
        UpdateService::class => null,
    ];

    public function getSettingService(): SettingService
    {
        return $this->getService(SettingService::class);
    }
}
