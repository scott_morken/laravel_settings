<?php

namespace Smorken\Settings\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Setting
 *
 *
 * @method static mixed get(string $key, mixed $default = null)
 * @method static bool set(string $key, mixed $value)
 */
class Setting extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return \Smorken\Settings\Contracts\Storage\Setting::class;
    }
}
