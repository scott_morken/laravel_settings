<?php

namespace Smorken\Settings\Contracts\Services;

interface CrudServices extends \Smorken\Service\Contracts\Services\CrudServices
{
    public function getSettingService(): SettingService;
}
