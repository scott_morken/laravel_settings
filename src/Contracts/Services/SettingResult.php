<?php

namespace Smorken\Settings\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property string $key
 * @property mixed $value
 */
interface SettingResult extends VOResult
{
}
