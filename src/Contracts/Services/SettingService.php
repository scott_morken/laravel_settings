<?php

namespace Smorken\Settings\Contracts\Services;

use Smorken\ArrayCache\Contracts\HasArrayCacheService;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Settings\Contracts\Storage\Setting;

interface SettingService extends BaseService, HasArrayCacheService
{
    public function flushCache(): bool;

    public function get(string $key, mixed $default = null): SettingResult;

    public function getProvider(): Setting;

    public function set(string $key, mixed $value): bool;
}
