<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:55 AM
 */

namespace Smorken\Settings\Contracts\Storage;

interface Setting
{
    public function flush(string $key): bool;

    public function flushAll(): bool;

    /**
     * @param  null  $default
     */
    public function get(string $key, mixed $default = null): mixed;

    public function set(string $key, mixed $value): bool;
}
