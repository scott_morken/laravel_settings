<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:59 AM
 */

namespace Smorken\Settings\Storage\Eloquent;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class Setting extends Base implements \Smorken\Settings\Contracts\Storage\Setting
{
    protected array $validationRules = [
        'key' => 'max:32',
        'descr' => 'max:64',
        'value' => 'required',
    ];

    public function all(): Collection|iterable
    {
        return $this->getCacheAssist()->remember(
            ['all'],
            Carbon::now()->addHour(),
            fn () => $this->getModel()
                ->newQuery()
                ->orderKey()
                ->get()
        );
    }

    public function flush(string $key): bool
    {
        return $this->getCacheAssist()->forget(['setting', $key]);
    }

    public function flushAll(): bool
    {
        /** @var \Smorken\Settings\Contracts\Models\Setting $setting */
        foreach ($this->all() as $setting) {
            $this->flush($setting->key);
        }
        $this->getCacheAssist()->forgetAuto();

        return true;
    }

    public function get(string $key, mixed $default = null): mixed
    {
        return $this->getCacheAssist()->remember(
            ['setting', $key],
            Carbon::now()->addHour(),
            function () use ($key, $default) {
                $m = $this->getModel()
                    ->newQuery()
                    ->keyIs($key)
                    ->first();
                if ($m) {
                    return $m->value;
                }

                return $default;
            }
        );
    }

    public function set(string $key, mixed $value): bool
    {
        $this->flushAll();
        $data['value'] = $value;
        $m = $this->getModel()
            ->newQuery()
            ->keyIs($key)
            ->first();
        if (! $m) {
            $data = [
                'key' => $key,
                'descr' => $key,
                'value' => $value,
            ];
            $m = $this->getModel()
                ->newInstance();
        }
        $m->fill($data);

        return $m->save();
    }

    protected function cacheForgetByModel($model): void
    {
        $this->flush($model->key);
        parent::cacheForgetByModel($model);
    }
}
