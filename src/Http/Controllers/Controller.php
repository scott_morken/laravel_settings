<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:03 AM
 */

namespace Smorken\Settings\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Controller\View\WithService\CrudController;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\Settings\Contracts\Services\CrudServices;
use Smorken\Settings\Contracts\Services\IndexService;
use Smorken\Settings\Contracts\Services\SettingService;

class Controller extends CrudController
{
    protected string $baseView = 'smorken/settings::setting';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }

    public function clearCache(Request $request): RedirectResponse
    {
        if ($this->getSettingService()->flushCache()) {
            $request->session()->flash('flash:info', 'Settings cache cleared.');
        } else {
            $request->session()->flash('flash:info', 'Settings cache was not cleared or was empty.');
        }

        return (new RedirectActionResult($this->actionArray('index')))->redirect();
    }

    protected function getSettingService(): SettingService
    {
        return $this->crudServices->getSettingService();
    }
}
