<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 8/18/16
 * Time: 8:04 AM
 */

namespace Smorken\Settings;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Smorken\ArrayCache\Contracts\Service;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Settings\Contracts\Services\CrudServices;
use Smorken\Settings\Contracts\Services\IndexService;
use Smorken\Settings\Contracts\Services\SettingService;
use Smorken\Settings\Contracts\Storage\Setting;
use Smorken\Settings\Routes\AdminRoutes;
use Smorken\Support\Contracts\LoadRoutes;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public static function rawRoutes(Application $app, string $routes = 'admin'): LoadRoutes
    {
        $map = [
            'admin' => (new AdminRoutes())
                ->setApp($app)
                ->controller($app['config']->get('settings.controller',
                    \Smorken\Settings\Http\Controllers\Controller::class)),
        ];

        return $map[$routes];
    }

    public function boot(): void
    {
        $this->bootViews();
        $this->bootConfig();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutes();
    }

    public function loadRoutes(): void
    {
        if ($this->app['config']->get('settings.load_routes', true)) {
            $prefix = implode('/', array_filter([$this->app['config']->get('settings.subnav', 'admin'), 'settings']));
            (new AdminRoutes())
                ->controller($this->app['config']->get('settings.controller',
                    \Smorken\Settings\Http\Controllers\Controller::class))
                ->prefix($prefix)
                ->middleware($this->app['config']->get('settings.middleware', ['web', 'auth', 'can:role-admin']))
                ->load($this->app);
        }
    }

    public function register(): void
    {
        $this->bindProvider();
        $this->bindServices();
    }

    protected function bindProvider(): void
    {
        $this->app->bind(
            Setting::class,
            function ($app) {
                $modelClass = $app['config']->get('settings.model', \Smorken\Settings\Models\Eloquent\Setting::class);
                $providerClass = $app['config']->get('settings.provider',
                    \Smorken\Settings\Storage\Eloquent\Setting::class);
                $m = new $modelClass();

                return new $providerClass($m, [
                    'forgetAuto' => [['all']],
                ]);
            }
        );
    }

    protected function bindServices(): void
    {
        $this->app->bind(IndexService::class, fn ($app) => new \Smorken\Settings\Services\IndexService($app[Setting::class]));
        $this->app->bind(CrudServices::class, function ($app) {
            $cs = \Smorken\Settings\Services\CrudServices::createByStorageProvider(
                $app[Setting::class],
                [
                    RequestService::class => new \Smorken\Settings\Services\RequestService(),
                    ValidatorService::class => new \Smorken\Service\Services\ValidatorService(
                        $app[Factory::class]
                    ),
                ]
            );
            $cs->setService(SettingService::class, $app[SettingService::class]);

            return $cs;
        });
        $this->app->bind(SettingService::class, fn ($app) => new \Smorken\Settings\Services\SettingService(
            $app[Setting::class],
            [
                Service::class => $app[Service::class],
            ]
        ));
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'settings');
        $this->publishes([__DIR__.'/../config/config.php' => config_path('settings.php')], 'config');
    }

    protected function bootViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'smorken/settings');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('views/vendor/smorken/settings'),
            ],
            'views'
        );
    }
}
