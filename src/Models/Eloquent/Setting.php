<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 12/20/16
 * Time: 7:57 AM
 */

namespace Smorken\Settings\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;

class Setting extends Base implements \Smorken\Settings\Contracts\Models\Setting
{
    protected $fillable = ['key', 'descr', 'value'];

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('key');
    }

    public function scopeKeyIs(Builder $query, string $key): Builder
    {
        return $query->where('key', '=', $key);
    }

    public function scopeOrderKey(Builder $q): Builder
    {
        return $q->orderBy('key');
    }

    public function setKeyAttribute(string $value): void
    {
        $this->attributes['key'] = preg_replace('[^A-z0-9\.\-_]', '', $value);
    }
}
