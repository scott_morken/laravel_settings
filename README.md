## Laravel 6 Settings package

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.2+
* [Composer](https://getcomposer.org/)

#### Installation

* add to your Laravel app composer.json

```
"require": {
    "smorken/settings": "^6.0"
}
```

* `composer update`

Service Provider should auto register itself. If not:

* add service provider to `config/app.php`

```
'providers' => [
...
    \Smorken\Settings\ServiceProvider::class,
```

* publish any needed files

```
$ php artisan vendor:publish --provider="Smorken\Settings\ServiceProvider" --tag=config #view and config available
```

* run the migrations

```
$ php artisan migrate
```

#### Use

`app(Smorken\Settings\Contracts\Storage\Setting::class)` provides an instance of `Smorken\Settings\Contracts\Storage\Setting`

`setting()` is a shortcut for the same

`Smorken\Settings\Facades\Setting::class` is a facade accessor

Get a setting value:

```
$s = app(Smorken\Settings\Contracts\Storage\Setting::class);
$value = $s->get('foo.key');

$value = setting('foo.key', 'default_value');

$value = \Smorken\Settings\Facades\Setting::get('foo.key', 'default_value');
```

Set a settings value (shortcut):

```
$s = app(Smorken\Settings\Contracts\Storage\Setting::class);
$s->set('foo.key', 'value');

setting()->set('foo.key', 'value');

\Smorken\Settings\Facades\Setting::set('foo.key', 'value');
```
