<?php

namespace Database\Factories\Smorken\Settings\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Smorken\Settings\Models\Eloquent\Setting;

class SettingFactory extends Factory
{
    protected $model = Setting::class;

    public function definition(): array
    {
        return [
            'key' => Str::random(10),
            'descr' => $this->faker->words(3, true),
            'value' => $this->faker->words(10, true),
        ];
    }
}
