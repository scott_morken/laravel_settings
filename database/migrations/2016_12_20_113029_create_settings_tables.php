<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    protected array $tables = [
        'settings',
    ];

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        foreach ($this->tables as $table) {
            Schema::drop($table);
        }
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        $this->settings();
    }

    protected function settings(): void
    {
        Schema::create(
            'settings',
            function (Blueprint $table) {
                $table->increments('id');
                $table->string('key', 32);
                $table->string('descr', 64);
                $table->text('value');
                $table->timestamps();

                $table->unique('key', 'settings_key_ndx');
            }
        );
    }
};
