@extends('layouts.app')
@section('content')
    <?php $params = [$model->getKeyName() => $model->getKey()];
    $first_col = 'col-sm-2';
    $second_col = 'col-sm-10';
    ?>
    <div class="mb-2 float-right float-end">
        <a href="{{ action([$controller, 'index'], isset($filter)?$filter->all():[]) }}" title="Back to list">Back</a>
    </div>
    <h4>Setting #{{ $model->getKey() }}</h4>
    <div class="row mb-4">
        <div class="col">
            <a href="{{ action([$controller, 'update'], $params) }}" title="Update {{ $model->getKey() }}"
               class="btn btn-primary btn-block w-100">Update</a>
        </div>
        <div class="col">
            <a href="{{ action([$controller, 'delete'], $params) }}" title="Delete {{ $model->getKey() }}"
               class="btn btn-danger btn-block w-100">Delete</a>
        </div>
    </div>
    <div class="row mb-2">
        <div class="{{ $first_col }} font-weight-bold">{{ $model->getKeyName() }}</div>
        <div class="{{ $second_col }}">{{ $model->getKey() }}</div>
    </div>
    <div class="row mb-2">
        <div class="{{ $first_col }} font-weight-bold">Key</div>
        <div class="{{ $second_col }}">{{ $model->key }}</div>
    </div>
    <div class="row mb-2">
        <div class="{{ $first_col }} font-weight-bold">Description</div>
        <div class="{{ $second_col }}">{{ $model->descr }}</div>
    </div>
    <div class="row mb-2">
        <div class="{{ $first_col }} font-weight-bold">Value</div>
        <div class="{{ $second_col }}">{{ $model->value }}</div>
    </div>
@endsection
