@extends('layouts.app')
@section('content')
    <h5 class="mb-2">Delete record [{{ $model->getKey() }}]</h5>
    <div class="alert alert-danger">
        <div>Are you sure you want to delete this record?</div>
        <div class="row mb-2">
            <div class="col-sm-2 font-weight-bold">ID</div>
            <div class="col-sm-10">{{ $model->getKey() }}</div>
        </div>
        <div class="row mb-2">
            <div class="col-sm-2 font-weight-bold">Key</div>
            <div class="col-sm-10">{{ $model->key }}</div>
        </div>
        <div class="row mb-2">
            <div class="col-sm-2 font-weight-bold">Value</div>
            <div class="col-sm-10">{{ $model->value }}</div>
        </div>
    </div>
    <form method="POST">
        @method('DELETE')
        @csrf
        <div>
            <button type="submit" class="btn btn-danger">Delete</button>
            <a href="{{ action([$controller, 'index']) }}"
               title="Cancel" class="btn btn-outline-primary">Cancel</a>
        </div>
    </form>
@endsection
