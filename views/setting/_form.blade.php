<form method="post" action="{{ action([$controller, 'doSave'], [$model->getKeyName() => $model->getKey()]) }}">
    @csrf
    @if (isset($create))
        <div class="form-group mb-2">
            <label for="key" class="form-label">Key</label>
            <input type="text" name="key" id="key" class="form-control" maxlength="32"
                   value="{{ old('key', $model->key) }}">
        </div>
    @endif
    <div class="form-group mb-2">
        <label for="descr" class="form-label">Description</label>
        <input type="text" name="descr" id="descr" class="form-control" maxlength="64"
               value="{{ old('descr', $model->descr) }}">
    </div>
    <div class="form-group mb-2">
        <label for="value" class="form-label">Value</label>
        <textarea class="form-control" rows="5" name="value" id="value"
                  class="form-control">{{ old('value', $model->value) }}</textarea>
    </div>
    <div class="">
        <button type="submit" class="btn btn-primary">Save</button>
        <a href="{{ action([$controller, 'index']) }}" title="Cancel" class="btn btn-outline-secondary">Cancel</a>
    </div>
</form>
