@extends('layouts.app')
@section('content')
    <h4>Settings</h4>
    <div>
        <a href="{{ action([$controller, 'create']) }}" title="Create new"
           class="btn btn-outline-success btn-sm mb-1 float-md-right float-md-end">New</a>
    </div>
    @if ($models && count($models))
        <table class="table table-striped mb-2">
            <thead>
            <tr>
                <th>ID</th>
                <th>Key</th>
                <th>Description</th>
                <th>Value</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                    <?php $params = [$model->getKeyName() => $model->getKey()]; ?>
                <tr>
                    <td>
                        <a href="{{ action([$controller, 'view'], $params) }}" title="View {{ $model->getKey() }}">
                            {{ $model->getKey() }}
                        </a>
                    </td>
                    <td>{{ $model->key }}</td>
                    <td>{{ $model->descr }}</td>
                    <td>{{ $model->value }}</td>
                    <td>
                        <div class="text-md-right text-md-end">
                            <a href="{{ action([$controller, 'update'], $params) }}" class="text-primary mr-5 me-5"
                               title="Update {{ $model->getKey() }}">update</a>
                            <a href="{{ action([$controller, 'delete'], $params) }}" class="text-danger"
                               title="Delete {{ $model->getKey() }}">delete</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="mb-2">
            <a href="{{ action([$controller, 'clearCache']) }}" title="Clear setting cache"
            >Clear Settings Cache</a>
        </div>
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
